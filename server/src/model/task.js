const mongoose = require("mongoose");

const PRIORITY = Object.freeze({"XLOW": 0, "LOW": 1, "MEDIUM": 2, "HIGH": 3, "XHIGH": 4});

const TaskSchema = mongoose.Schema({
    title: String, 
    subtitle: String,
    description: String,
    created: { type: Date, default: Date.now() },
    until: { type: Date },
    done: Boolean,
    priority: { type: Number, default: PRIORITY.MEDIUM },
    parent: { type: mongoose.Schema.Types.ObjectId, ref: "Task"},
    children: [{ type: mongoose.Schema.Types.ObjectId, ref: "Task" }],
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
}, {
    timestamps: true
});

module.exports = mongoose.model("Task", TaskSchema);
