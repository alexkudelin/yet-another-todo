const mongoose = require("mongoose");

const SessionSchema = mongoose.Schema({
    sid: { type: String, required: true, unique: true },
    login: { type: String, required: true }
});

module.exports = mongoose.model("Session", SessionSchema);
