module.exports = (app) => {
    const user = require("../controller/user");

    app.get("/user/");
    app.get("/user/details/", user.getInfo);
}