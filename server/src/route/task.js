const auth = require("../controller/auth");

module.exports = (app) => {
    const task = require("../controller/task");

    app.get("/tasks/", auth.wrapper(task.getAll));
    app.post("/tasks/", auth.wrapper(task.add));

    app.get("/tasks/:taskId/", auth.wrapper(task.getOne));
    app.post("/tasks/:taskId/", auth.wrapper(task.update));

    app.post("/tasks/:taskId/updateState/", auth.wrapper(task.updateState));

    app.delete("/tasks/", auth.wrapper(task.deleteAll));
    app.delete("/tasks/:taskId/", auth.wrapper(task.deleteOne));
}