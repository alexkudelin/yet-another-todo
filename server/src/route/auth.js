module.exports = (app) => {
    const user = require("../controller/user");

    app.post("/auth/signup", user.signup);
    app.post("/auth/login", user.login);
    app.post("/auth/logout", user.logout);
}