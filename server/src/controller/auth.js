const Session = require('./session.js');

exports.auth = (req, res, next) => {
    if (req.cookies.sid) {
        Session.getLogin(req.cookies.sid)
            .then(login => {
                req.login = login;
                next();
            })
            .catch(_err => {
                next();
            });
    } else {
        next();
    }
}

exports.wrapper = (next) => {
    return (req, res) => {
        if (req.login) {
            next(req, res);
        } else {
            res.status(401).send('You need to authorize to perform this action.');
        }
    }
}