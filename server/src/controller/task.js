const Task = require("../model/task");
const Session = require("../controller/session");
const User = require("../model/user");

exports.add = (req, res) => {
    Session.getLogin(req.cookies.sid)
        .then(login => {
            User.findOne({ login })
                .then(user => {
                    const { 
                        title,
                        subtitle,
                        description,
                        created,
                        until,
                        priority,
                    } = req.body;

                    const curTask = new Task({
                        title: title,
                        subtitle: subtitle,
                        priority: priority,
                        title: title,
                        subtitle: subtitle,
                        description: description,
                        created: created,
                        until: until,
                        priority: priority,
                        parent: null,
                        done: false,
                        children: [],
                        user: user
                    });

                    curTask.save();

                    res.status(200).send(curTask);
                })
                .catch(error => {
                    res.status(500).send("Не удалось найти пользователя по логину: " + error);
                })        
        })
        .catch(error => {
            res.status(500).send("Не удалось найти логин пользователя: " + error);
        })
};

exports.deleteAll = (req, res) => {
    Session.getLogin(req.cookies.sid)
        .then(login => {
            User.findOne({ login })
                .then(user => {
                    Task.deleteMany({ user: user })
                        .then(tasks => {
                            res.status(200).send(tasks)
                        })
                })
        })
}

exports.deleteOne = (req, res) => {
    Session.getLogin(req.cookies.sid)
        .then(login => {
            User.findOne({ login })
                .then(user => {
                    Task.findByIdAndRemove(req.params.taskId)
                        .then(curTask => {
                            res.status(200).send(curTask)
                        })
                })
        })
};

exports.getAll = (req, res) => {
    Session.getLogin(req.cookies.sid)
        .then(login => {
            User.findOne({ login })
                .then(user => {
                    Task.find({ user: user })
                        .sort({ priority: "descending" })
                        .exec((error, tasks) => {
                            if (error) {
                                res.status(500).send({ message: error });
                            } else {
                                res.status(200).send(tasks);
                            }
                        })
                })
        })
}

exports.getOne = (req, res) => {
    Task.findById(req.params.taskId)
        .then(task => {
            res.status(200).send(task);
        });
}

exports.update = (req, res) => {
    updTask = {
        title: req.body.task.title,
        subtitle: req.body.task.subtitle,
        description: req.body.task.description,
        priority: req.body.task.priority
    }

    Task.findByIdAndUpdate({ _id: req.params.taskId }, updTask,  (err, doc) => {
        if (err) {
            res.status(500).send({ message: "Не удалось сохранить изменения" })
        } else {
            res.status(200).send(doc);
        }
    })
}

exports.updateState = (req, res) => {
    newState = {
        done: req.body.done,
    }

    Task.findByIdAndUpdate({ _id: req.params.taskId }, newState, (err, task) => {
        if (err) {
            res.status(500).send({ message: "Не удалось сохранить изменения" });
        } else {
            res.status(200).send(task);
        }
    })
}
