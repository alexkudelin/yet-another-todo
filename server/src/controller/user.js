const User = require("../model/user");
const Session = require("../controller/session");
const bcrypt = require("bcrypt");

exports.signup = (req, res) => {
    const { login, password, firstname, lastname } = req.body;

    User.findOne({ login: login })
        .then(tempUser => {
            if (tempUser) {
                res.status(500).send({ "message": "Данный логин занят" });
            }
            else {
                const user = new User({ login, password, firstname, lastname });

                user.save()
                    .then(userInfo => {
                        res.status(200).send(userInfo);
                    }).catch(err => {
                        res.status(500).send({
                            message: err || "Error registering new user please try again."
                        });
                    });
            }
        })
}

exports.login = (req, res) => {
    const { login, password } = req.body;

    User.findOne({ login: login })
        .then(user => {
            if (bcrypt.compare(password, user.password, (error, result) => {
                if (error) {
                    res.status(500).send({ message: "Ошибка при проверке пароля" });
                } else if (result) {
                    Session.add(login)
                        .then(sid => {
                            res.cookie("sid", sid, { httpOnly: false });
                            res.status(200).send({ sid: sid });
                        })
                        .catch(error => {
                            res.status(500).send({ message: "Не удалось авторизоваться, сессия уже занята" });
                        });
                } else {
                    res.status(500).send({ message: "Пароли не совпадают" });
                }
            }));
        })
        .catch(error => {
            res.status(500).send({ message: `Пользователь ${login} не найден` });
        });
}

exports.logout = (req, res) => {
    if (req.cookies.sid) {
        Session.delete(req.cookies.sid)
            .then(sid => {
                res.clearCookie(sid);
                res.status(200).send({ message: "Сессия завершена" });
            })
            .catch(error => {
                res.status(500).send({ message: "Ошибка при попытке завершить сессию пользователя" });
            });
    } else {
        res.status(500).send({ message: "Пользователь не авторизован" });
    }
}

exports.getInfo = (req, res) => {
    if (req.cookies.sid) {
        Session.getLogin(req.cookies.sid)
            .then(login => {
                User.findOne({login: login})
                    .then(user => {
                        if (user) {
                            res.status(200).send(user);
                        } else {
                            res.status(500).send({ message: "Не удалось найти пользователя по sid" });
                        }
                    })
            })
            .catch(error => {
                res.status(500).send({ message: error });
            })
    }
}
