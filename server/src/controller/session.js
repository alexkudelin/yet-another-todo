const Session = require('../model/session.js');
const uniqid = require('uniqid');

exports.getLogin = (sid) => {
    return new Promise((resolve, reject) => {
        Session.findOne({ sid: sid })
            .then((session) => {
                resolve(session.login);
            })
            .catch(error => {
                reject(error);
            });
    });
}

exports.add = (login) => {
    const sid = uniqid();
    const session = new Session({ sid, login });

    return new Promise((resolve, reject) => {
        session.save()
            .then(() => {
                resolve(sid);
            }).catch(error => {
                reject(error);
            });
    });
}

exports.delete = (sid) => {
    return new Promise((resolve, reject) => {
        Session.deleteOne({ sid: sid })
            .then(() => {
                resolve(sid);
            })
            .catch(error => {
                reject(error);
            })
    });
}