const express = require("express");
const bodyparser = require("body-parser");
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const auth = require("./src/controller/auth");

const router = express.Router();
const app = express();
const API_PORT = 3001;

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(cookieParser());
app.use(auth.auth);

app.use("/api/v1", router);

require("./src/route/task")(router);
require("./src/route/user")(router);
require("./src/route/auth")(router);

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect("mongodb://localhost/yetanothertodo", {
    useNewUrlParser: true
}).then(() => {
    console.log("Connected to DB ...");
}).catch(err => {
    console.log("Disconnected from DB ...", err);
    process.exit();
});

app.listen(API_PORT,  () => {
   console.log("App is running on port " + API_PORT);
});
