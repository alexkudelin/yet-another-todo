import React, { Component } from "react";
import Cookies from 'js-cookie';
import UserAPI from "../api/user";

class LoginPage extends Component {
    state = {
        login: "",
        password: ""
    };

    loginChangeHandler = (event) => {
        this.setState({ login: event.target.value });
    }

    passwordChangeHandler = (event) => {
        this.setState({ password: event.target.value });
    }

    loginButtonClickHandler = () => {
        UserAPI.login(this.state.login, this.state.password)
            .then(response => {
                response.json()
                    .then(data => {
                        Cookies.set("sid", data.sid);

                        this.setState({
                            login: "",
                            password: ""
                        });

                        this.props.history.push("/");
                    })
            })
            .catch(data => {
                data.json()
                    .then(data => {
                        this.setState({
                            login: "",
                            password: "",
                            loginError: data.message
                        })
                    })
            })
    }

    render() {
        let loginError;

        if (this.state.loginError) {
            loginError = (<div className="alert alert-danger" role="alert">{this.state.loginError}</div>)
        }

        return (
            <div className="container center-div">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <div className="jumbotron" >
                            <div className="container">
                                <div className="row">
                                    <div className="col col-12">
                                        <h2>Войти в систему</h2><br />
                                        {loginError}

                                        <input
                                            onChange={this.loginChangeHandler}
                                            value={this.state.login}
                                            className="form-control mr-sm-2"
                                            type="text"
                                            placeholder="Логин"
                                        />
                                        <br />
                                        <input
                                            onChange={this.passwordChangeHandler}
                                            value={this.state.password}
                                            className="form-control mr-sm-2"
                                            type="password"
                                            placeholder="Пароль"
                                        />
                                        <br />
                                        <div className="row">
                                            <div className="col-12">
                                                <button
                                                    onClick={this.loginButtonClickHandler}
                                                    className="btn btn-primary btn-lg btn-block">Войти
                                                </button>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="row">
                                            <div className="col-12">
                                                <a
                                                    href="/auth/signup"
                                                    className="btn btn-outline-primary btn-lg btn-block">Регистрация
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )};
}

export default LoginPage;
