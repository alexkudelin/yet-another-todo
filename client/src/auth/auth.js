import React, { Component } from "react";
import { Link } from "react-router-dom";

class AuthPage extends Component {
    render() {
        return (
            <div className="container center-div">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <div className="jumbotron" >
                            <div className="container">
                                <div className="row">
                                    <div className="col col-12">
                                        <h2>Hello! Put another one note :)</h2><br />
                                        <div className="row">
                                            <div className="col-12">
                                                <Link
                                                    to="/auth/login"
                                                    className="btn btn-primary btn-lg btn-block">Войти
                                                </Link>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="row">
                                            <div className="col-12">
                                                <Link
                                                    to="/auth/signup"
                                                    className="btn btn-outline-primary btn-lg btn-block">Регистрация
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )};
}

export default AuthPage;
