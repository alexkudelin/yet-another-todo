import React, { Component } from "react";
import UserAPI from "../api/user";
import Cookies from "js-cookie";

class SignUpPage extends Component {
    state = {};

    constructor(props) {
        super(props);

        this.state.login = "";
        this.state.password = "";
        this.state.firstname = "";
        this.state.lastname = "";
    }

    loginChangeHandler = (event) => {
        this.setState({ login: event.target.value });
    }

    passwordChangeHandler = (event) => {
        this.setState({ password: event.target.value });
    }

    firstnameChangeHandler = (event) => {
        this.setState({ firstname: event.target.value });
    }

    lastnameChangeHandler = (event) => {
        this.setState({ lastname: event.target.value });
    }

    signupButtonClickHandler = () => {
        UserAPI.signup(this.state.login, this.state.password, this.state.firstname, this.state.lastname)
            .then(response => response.json())
            .then(_ => {
                UserAPI.login(this.state.login, this.state.password)
                    .then(response => response.json())
                    .then(data => {
                        Cookies.set("sid", data.sid);
        
                        this.setState({
                            login: "",
                            password: ""
                        });
            
                        this.props.history.push("/");
                    })
                    .catch(data => {
                        data.json()
                            .then(data => {
                                this.setState({
                                    login: "",
                                    password: "",
                                    signupError: data.message
                                })
                            })
                    })
                })
            .catch(data => {
                data.json()
                    .then(data => {
                        this.setState({
                            login: "",
                            password: "",
                            signupError: data.message
                        })
                    })
            })
    }

    render() {
        let signupError;

        if (this.state.signupError) {
            signupError = (<div className="alert alert-danger" role="alert">{this.state.signupError}</div>)
        }

        return (
            <div className="container center-div">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <div className="jumbotron" >
                            <div className="container">
                                <div className="row">
                                    <div className="col col-12">
                                        <h3>Зарегистрироваться в системе</h3><br />
                                        {signupError}

                                        <input
                                            onChange={this.loginChangeHandler}
                                            value={this.state.login}
                                            className="form-control mr-sm-2"
                                            type="text"
                                            placeholder="Логин"
                                            required
                                        />
                                        <br />
                                        <input
                                            onChange={this.passwordChangeHandler}
                                            value={this.state.password}
                                            className="form-control mr-sm-2"
                                            type="password"
                                            placeholder="Пароль"
                                            required
                                        />
                                        <br />
                                        <input
                                            onChange={this.firstnameChangeHandler}
                                            value={this.state.firstname}
                                            className="form-control mr-sm-2"
                                            type="text"
                                            placeholder="Имя"
                                        />
                                        <br />
                                        <input
                                            onChange={this.lastnameChangeHandler}
                                            value={this.state.lastname}
                                            className="form-control mr-sm-2"
                                            type="text"
                                            placeholder="Фамилия"
                                        />
                                        <br />
                                        <div className="row">
                                            <div className="col-12">
                                                <button
                                                    onClick={this.signupButtonClickHandler}
                                                    className="btn btn-primary btn-lg btn-block">Зарегистрироваться
                                                </button>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="row">
                                            <div className="col-12">
                                                <a
                                                    href="/auth/login"
                                                    className="btn btn-success btn-lg btn-block">Уже регистрировались?
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )};
}

export default SignUpPage;
