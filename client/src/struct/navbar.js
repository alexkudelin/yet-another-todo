import React, { Component } from "react";
import { Link } from "react-router-dom";
import UserAPI from "../api/user";
import Cookies from "js-cookie";

class Navbar extends Component {
    state = {}

    constructor(props) {
        super(props);
        this.state.firstname = ":-)";
        this.state.lastname = "";
        this.getName();
    }

    getName = () => {
        UserAPI.getInfo()
            .then(response => {
                response.json()
                    .then(data => {
                        this.setState({
                            firstname: data.firstname || this.state.firstname,
                            lastname: data.lastname,
                        })
                    })
            })
            .catch(data => {
                data.json()
                    .then(data => {
                        console.log(data);
                    })
            })
    }


    logoutButtonClickHandler = () => {
        UserAPI.logout()
            .then(response => {
                if (response.ok) {
                    Cookies.remove("sid");
                    this.props.history.push("/");
                }
            })
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">Yet Another Todo</Link>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <div className="dropdown">
                                <button className="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="material-icons align-bottom">account_circle</i>&nbsp;&nbsp;
                                        {this.state.firstname}&nbsp;&nbsp;
                                </button>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <Link className="dropdown-item" to="/user/">Профиль</Link>
                                    <div className="dropdown-divider"></div>
                                    <button
                                        onClick={this.logoutButtonClickHandler}
                                        className="dropdown-item"
                                        type="button"
                                    >Выйти
                                    </button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default Navbar;
