import React, { Component } from "react";
import TaskGrid from "../task/task_grid";

import Navbar from "./navbar";
import Controlbar from "./controlbar";

class RootPage extends Component {
    render() {
        return (
            <div>
                <Navbar {...this.props} />
                <Controlbar {...this.props} />

                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12 py-3">
                            <TaskGrid {...this.props}/>
                        </div>
                    </div>
                </div>
            </div>
    )};
}

export default RootPage;
