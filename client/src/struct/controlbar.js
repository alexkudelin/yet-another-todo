import React, { Component } from "react";

class Controlbar extends Component {
    state = {}

    handleCreateTask = (event) => {
        this.props.history.push("/tasks/add/");
    }

    handleDeleteAllTasks = (event) => {
        this.props.history.push("/tasks/deleteAll/")
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row bg-secondary py-1">
                    <div className="col col-md-12">
                        <button type="button" className="btn btn-secondary btn-sm" onClick={this.handleCreateTask}>
                            <i className="material-icons align-bottom">note_add</i>Добавить
                        </button>
                        <button type="button" className="btn btn-secondary btn-sm" onClick={this.handleDeleteAllTasks}>
                            <i className="material-icons align-bottom">delete</i>Удалить все
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Controlbar;
