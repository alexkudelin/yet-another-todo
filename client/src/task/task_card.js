import React, { Component } from "react";
import { Link } from "react-router-dom";
import TaskAPI from "../api/task";

class TaskCard extends Component {
    state = {
    }

    priorityColorMap = {
        0: ["bg-xlow", "bg-header-xlow"],
        1: ["bg-low", "bg-header-low"],
        2: ["bg-medium", "bg-header-medium"],
        3: ["bg-high", "bg-header-high"],
        4: ["bg-xhigh", "bg-header-xhigh"]
    }

    componentDidMount() {
        this.matchPriorityColor();
    }

    matchPriorityColor = () => {
        var colorPair = this.priorityColorMap[this.props.task.priority];

        this.setState({
                bgCardColor: colorPair[0],
                bgCardHeaderColor: colorPair[1]
        });
    }

    render() {
        let bgCardColor = "card " + this.state.bgCardColor;
        let bgCardHeaderColor = "card-header " + this.state.bgCardHeaderColor;

        return (
            <div>
                <div className={bgCardColor}>
                    <h4 className={bgCardHeaderColor}><b>{this.props.task.title}</b></h4>
                    <div className="card-body">
                        <h5 className="card-title"><i>{this.props.task.subtitle}</i></h5>
                        <h6 className="card-text">{this.props.task.description}</h6>
                    </div>
                    <div className={bgCardHeaderColor} align="right">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            <Link
                                to={"/tasks/" + this.props.task._id + "/updateState/" + (this.props.task.done ? "false/" : "true/")}
                                className={"btn btn-" + (this.props.task.done ? "primary" : "success") + " border border-dark"}>
                                <i className="material-icons align-bottom">{this.props.task.done ? "close" : "done"}</i>
                            </Link>
                            <Link to={"/tasks/" + this.props.task._id + "/edit/"} className="btn btn-warning border border-dark">
                                <i className="material-icons align-bottom">edit</i>
                            </Link>
                            <Link to={"/tasks/" + this.props.task._id + "/delete/"} className="btn btn-danger border border-dark">
                                <i className="material-icons align-bottom">delete</i>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TaskCard;