import React, { Component } from "react";
import TaskAPI from "../api/task";
import TaskCard from "./task_card";

class TaskGrid extends Component {
    state = {
        tasks: []
    }

    constructor(props) {
        super(props);
        this.getTasks();
    }

    getTasks = () => {
        TaskAPI.get()
            .then(response => response.json())
            .then(data => {
                this.setState({
                    tasks: data
                })
            });
    }

    render() {
        var doneTasks = [], undoneTasks = [];

        this.state.tasks.forEach((task) => {
            if (task.done) {
                doneTasks.push(task);
            } else {
                undoneTasks.push(task);
            }
        })

        var undoneChunks = [], doneChunks = [], chunkSize = 6;

        for (var i = 0; i < undoneTasks.length; i += chunkSize) {
            undoneChunks.push(undoneTasks.slice(i, i + chunkSize));
        }

        for (i = 0; i < doneTasks.length; i += chunkSize) {
            doneChunks.push(doneTasks.slice(i, i + chunkSize));
        }

        var undoneDiv = undoneChunks.map((chunk, chunkIdx) => {
                return (
                    <div>
                        <div className="row">
                            {
                                chunk.map((task, index) => {
                                    return <div className="col col-md-2"><TaskCard {...this.props} task={task} key={chunkIdx * chunkSize + index}/></div>
                                })
                            }
                        </div><br/>
                    </div>
                )
            });

        var doneDiv = doneChunks.map((chunk, chunkIdx) => {
                return (
                    <div>
                        <div className="row">
                            {
                                chunk.map((task, index) => {
                                    return <div className="col col-md-2"><TaskCard {...this.props} task={task} key={chunkIdx * chunkSize + index}/></div>
                                })
                            }
                        </div><br/>
                    </div>
                )
            });

        return (
            <div>
                {undoneDiv}
                <hr/>
                {doneDiv}
            </div>
        );
    }
}

export default TaskGrid;
