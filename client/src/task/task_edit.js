import React, { Component } from "react";
import TaskAPI from "../api/task";
import Navbar from "../struct/navbar";
import Controlbar from "../struct/controlbar";

class TaskEditPage extends Component {
    state = {
    }

    priorityColorMap = {
        0: ["bg-xlow", "bg-header-xlow"],
        1: ["bg-low", "bg-header-low"],
        2: ["bg-medium", "bg-header-medium"],
        3: ["bg-high", "bg-header-high"],
        4: ["bg-xhigh", "bg-header-xhigh"]
    }

    priorityList = [
        "Extra low",
        "Low",
        "Medium",
        "High",
        "Extra high"
    ]

    componentWillMount() {
        this.setState({
            task: {
                title: "",
                subtitle: "",
                description: "",
                priority: 0
            },
            isDataFetched: true,
            isNew: this.props.match.params.taskId == null
        });
    }
    
    componentDidMount() {
        if (this.props.match.params.taskId) {
            this.getTask();
        } else {
            this.matchPriorityColor();
        }
    }

    getTask = () => {
        TaskAPI.getOne(this.props.match.params.taskId)
            .then(response => response.json())
            .then(task => {
                this.setState({
                    task: task,
                    isDataFetched: true
                });

                this.matchPriorityColor();
            });
    }

    matchPriorityColor = () => {
        if (this.state.task) {
            let colorPair = this.priorityColorMap[this.state.task.priority];

            this.setState({
                bgCardColor: colorPair[0],
                bgCardHeaderColor: colorPair[1]
            })
        }
    }

    
    onClickLinkToParent = () => {
    }

    onUpdateOrSave = (event) => {
        event.preventDefault();

        var func = this.state.isNew ? TaskAPI.add : TaskAPI.update;

        func(this.state.task)
            .then(response => response.json())
            .then(data => {
                this.props.history.push("/");
            })
            .catch(data => {
                console.log(data.message);
            })
        }
    
    titleChangeHandler = (event) => {
        var tempState = {...this.state.task};
        tempState.title = event.target.value;

        this.setState({ task: tempState });
    }

    subtitleChangeHandler = (event) => {
        var tempState = {...this.state.task};
        tempState.subtitle = event.target.value;

        this.setState({ task: tempState });
    }
    
    descriptionChangeHandler = (event) => {
        var tempState = {...this.state.task};
        tempState.description = event.target.value;
        
        this.setState({ task: tempState });
    }

    priorityChangeHandler = (event) => {
        var tempState = {...this.state.task};
        tempState.priority = event.target.value;

        this.setState({ task: tempState });

        this.refs.priorityMarker.className = "input-group-text " + this.priorityColorMap[event.target.value][0];
    }
    
    render() {
        if (!this.state.isDataFetched) return (<div><Navbar/><Controlbar/></div>);

        let controlbar = this.state.isNew ? null : <Controlbar {...this.props}/>;

        return (
            <div>
                <Navbar {...this.props}/>
                {controlbar}

                <div className="container center-div">
                    <div className="row">
                        <div className="col-md-6 offset-md-3">
                            <div className="jumbotron" >
                                <div className="container">
                                    <div className="row">
                                        <div className="col col-12">
                                            <form>
                                                <div className="form-group">
                                                    <label>Заголовок</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="Заголовок..."
                                                        value={this.state.task.title}
                                                        onChange={this.titleChangeHandler}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Подзаголовок</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="Подзаголовок..."
                                                        value={this.state.task.subtitle}
                                                        onChange={this.subtitleChangeHandler}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Описание</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="Описание..."
                                                        value={this.state.task.description}
                                                        onChange={this.descriptionChangeHandler}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label>Приоритет</label>
                                                    <div className="input-group mb-3">
                                                        <div className="input-group-prepend">
                                                            <label className={"input-group-text " + this.state.bgCardColor} ref="priorityMarker">&nbsp;&nbsp;&nbsp;</label>
                                                        </div>
                                                        <select
                                                            className="custom-select"
                                                            onChange={this.priorityChangeHandler}
                                                            value={this.state.task.priority}
                                                        >
                                                            {
                                                                this.priorityList.map((value, i) => {
                                                                    return <option value={i} key={i}>{value}</option>
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                </div><br/>
                                                <div className="row">
                                                    <div className="col-12">
                                                        <button
                                                            onClick={this.onUpdateOrSave}
                                                            className={"btn btn-" + (this.state.isNew ? "success" : "warning") + " btn-lg btn-block" }
                                                        >
                                                            { this.state.isNew ? "Сохранить" : "Обновить" }
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TaskEditPage;