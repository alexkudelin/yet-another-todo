import React from "react";
import Cookies from 'js-cookie';

import LoginPage from "./auth/login";
import SignUpPage from "./auth/signup";
import AuthPage from "./auth/auth";
import RootPage from "./struct/main";
import UserPage from "./user/user";
import TaskEditPage from "./task/task_edit";
import TaskAPI from "./api/task";

import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

class App extends React.Component {
    state = {};

    isAuthorized() {
        return !!Cookies.get('sid');
    }

    render() {
        return (
            <Router>
                <div>
                    <Route
                        exact
                        path="/"
                        render={(props) => {
                            return this.isAuthorized() ? <RootPage  {...props} /> : <AuthPage {...props} />;
                        }}
                    />
                    <Route
                        exact
                        path="/auth/"
                        render={(props) => {
                            return this.isAuthorized() ? <Redirect to="/"/> : <AuthPage {...props} />
                        }}
                    />
                    <Route 
                        path="/auth/login/"
                        render={(props) => {
                            return this.isAuthorized() ? <Redirect to="/"/> : <LoginPage {...props} />;
                        }}
                    />
                    <Route
                        path="/auth/signup/"
                        render={(props) => {
                            return this.isAuthorized() ? <Redirect to="/" /> : <SignUpPage {...props} />;
                        }}
                    />
                    <Route
                        path="/user/"
                        render={(props) => {
                            return this.isAuthorized() ? <UserPage {...props} /> : <Redirect to="/" />;
                        }}
                    />
                    <Route
                        path="/tasks/:taskId/edit/"
                        render={(props) => {
                            return this.isAuthorized() ? <TaskEditPage {...props} /> : <Redirect to="/" />;
                        }}
                    />
                    <Route
                        path="/tasks/:taskId/delete/"
                        render={(props) => {
                            TaskAPI.delete(props.match.params.taskId)
                                .then(response => response.json())
                                .then(data => {
                                    props.history.push("/");
                                })
                            }}
                    />
                    <Route
                        path="/tasks/:taskId/updateState/:newState/"
                        render={(props) => {
                            TaskAPI.updateState(props.match.params.taskId, props.match.params.newState)
                                .then(response => response.json())
                                .then(data => {
                                    props.history.push("/");
                                })
                            }}
                    />
                    <Route
                        path="/tasks/add/"
                        render={(props) => {
                            return this.isAuthorized() ? <TaskEditPage {...props} /> : <Redirect to="/" />;
                        }}
                    />
                    <Route path="/tasks/deleteAll/"
                        render={(props) => {
                            TaskAPI.deleteAll()
                                .then(response => response.json())
                                .then(data => {
                                    props.history.push("/");
                                })
                            }}
                    />
                </div>
            </Router>
        )
    }
}

export default App;
