const API_URL = "https://yetanothertodo.me/api/v1/"

export default {
    get: () => {
        const requestUrl = API_URL + "tasks/";
        const requestParams = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            mode: "cors",
            credentials: "include",
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    getOne: (taskId) => {
        const requestUrl = API_URL + "tasks/" + taskId;
        const requestParams = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            mode: "no-cors",
            credentials: "include",
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    add: (task) => {
        const requestUrl = API_URL + "tasks/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            mode: "no-cors",
            credentials: "include",
            body: JSON.stringify({
                "title": task.title,
                "subtitle": task.subtitle,
                "description": task.description,
                "priority": task.priority
            }),
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    update: (task) => {
        const requestUrl = API_URL + "tasks/" + task._id + "/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            mode: "no-cors",
            credentials: "include",
            body: JSON.stringify({
                "task": {
                    "title": task.title,
                    "subtitle": task.subtitle,
                    "description": task.description,
                    "priority": task.priority
                }
            }),
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    updateState: (taskId, isDone) => {
        const requestUrl = API_URL + "tasks/" + taskId + "/updateState/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include",
            body: JSON.stringify({
                "done": isDone
            }),
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    delete: (taskId) => {
        const requestUrl = API_URL + "tasks/" + taskId + "/";
        const requestParams = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include",
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    deleteAll: () => {
        const requestUrl = API_URL + "tasks/";
        const requestParams = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            },
            credentials: "include",
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    }
}