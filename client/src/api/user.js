const API_URL = "https://yetanothertodo.me/api/v1/"

export default {
    signup: (login, password, firstname, lastname) => {
        const requestUrl = API_URL + "auth/signup/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            mode: "no-cors",
            body: JSON.stringify({
                "login": login,
                "password": password,
                "firstname": firstname,
                "lastname": lastname
            })
        };

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then((response) => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error))
        });
    },

    login: (login, password) => {
        const requestUrl = API_URL + "auth/login/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            mode: "no-cors",
            credentials: "include",
            body: JSON.stringify({
                login: login,
                password: password
            })
        }

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then(response => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(err => console.log(err));
        });
    },

    logout: () => {
        const requestUrl = API_URL + "auth/logout/";
        const requestParams = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Origin": "https://yetanothertodo.me"
            },
            mode: "no-cors",
            credentials: "include"
        }

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then(response => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error));
        });
    },

    getInfo: () => {
        const requestUrl = API_URL + "user/details/";
        const requestParams = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            },
            mode: "no-cors",
            credentials: "include"
        }

        return new Promise((resolve, reject) => {
            fetch(requestUrl, requestParams)
                .then(response => {
                    if (response.ok) {
                        resolve(response);
                    } else {
                        reject(response);
                    }
                })
                .catch(error => console.log(error));
        })
    }
}
